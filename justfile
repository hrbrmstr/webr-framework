bind_address := "127.0.0.1"
exposed_port := "9102"
container_port := "80"
image_version := "0.1.0"
image_name := "webr-framework"
container_name := "webr-framework-fwrk"

# Display this list
@default:
    just --list

# cleanup dist & node_modules
@clean-npm:
    rm -rf ./dist
    rm -rf ./node_modules

# Update NPM modules
@update:
    npm update

# Clean the cache
@clean:
    npm run clean

# build the report
@build: clean
    npm run build
    cp favicon.ico dist
    date > "dist/$(date)"

# start report dev server
@dev: clean
    npm run dev

# build the docker image
@build-image: build
    #!/bin/bash
    unset DOCKER_HOST
    docker build --tag {{ image_name }}:{{ image_version }} --tag {{ image_name }}:latest .

# deploy to Observable
@deploy:
    npm run deploy -- --message "cron"

# run the docker image
@daemon:
    #!/bin/bash
    unset DOCKER_HOST
    if [ $(docker inspect --format '\{\{.State.Running}}' {{ container_name }}) = "true" ] ; then
      docker stop {{ container_name }}
    fi
    docker rm --force {{ container_name }}
    docker image prune --force
    docker run \
      --restart unless-stopped \
      --detach \
      --publish {{bind_address}}:{{ exposed_port }}:{{ container_port }} \
      --name {{ container_name }} \
      {{ image_name }}:latest
